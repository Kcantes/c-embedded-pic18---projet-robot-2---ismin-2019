/* 
 * File:   global.h
 * Author: cheva
 *
 * Created on 9 mai 2019, 22:09
 */
//variables globales
#ifndef GLOBAL_H
#define	GLOBAL_H
extern unsigned char marche,nbVmesure;
extern volatile unsigned long vbat;//sum of vbats
extern unsigned int dutyCycleMoteurD,dutyCycleMoteurG;
extern unsigned float dutyCycleServo;
extern unsigned int timer1ticks;// nombre de ticks du timer en cours
extern unsigned int Tmr1StartTime;//d�but du timer 1
extern int tmp;// store fct results
extern volatile unsigned int led;
extern unsigned volatile char touche[3];//stores remote button value
#endif	/* GLOBAL_H */

