/*
 * File:   interruption.c
 * Author: cheva
 *
 * Created on 9 mai 2019, 22:01
 */

#include <p18cxxx.h>
#include "interruption.h"
#include "global.h"
#include "MI2C.h"
#include "coreFunctions.h"

volatile char str[30]="vbat=|0";
unsigned int i=0,j=0,k=0;//message inc vars
char val[12]="";//vbat value str
volatile unsigned long vreal=0;//vbat mid value
unsigned volatile char touche[3];
//high priority vector
#pragma code HighVector=0x08
void IntHighVector(void)
{
    _asm goto HighISR _endasm
}
#pragma code

#pragma interrupt HighISR
void HighISR(void)
{
    //IT t�l�commande
   if(INTCONbits.INT0IF)
   {
      INTCONbits.INT0IF = 0;
      //Ecrire_i2c_Telecom(0xA2, 0x31);//demande � la telecommande
      //while(Detecte_i2c(0xA2));//ack
      //Lire_i2c_Telecom(0xA2, touche);//lecture
      if(1||touche[1]==0x33){//bouton du milieu
        if(marche==0) marche = 1;
        else marche = 0;
      }
   }
   //IT Timer0
   if(INTCONbits.TMR0IF){
        INTCONbits.TMR0IF=0;
        TMR0H=0xE1;
        TMR0L=0x7B;
    //si le flag d'interruption est ON
        ADCON0bits.GO=1;
    }
   //Ecrire Vbat
   if(PIR1bits.TXIF&&PIE1bits.TXIE){
        PIR1bits.TXIF=0;
        if(str[i]!='|'){//vbat=
             TXREG=str[i];
             i++;
             k=(int)(12*(double)vreal/49088.0);
        }
        else{
            while(k!=0){//int to char
                val[j]=k%10+48;
                k/=10;
                j++;
            }
            if(j>0){//invert
                j--;
                TXREG=val[j];
            }
            else{//reinit + retour chariot
                TXREG=13;
                i=0;j=0;k=0;
                PIE1bits.TXIE=0;
            }
        }   
   }
   //IT ADC
   if(PIR1bits.ADIF==1)
   {
      ADCON0bits.GO=0;
      PIR1bits.ADIF=0;
      vbat+=ADRESH*256+ADRESL & 0x0000FFFF;
      nbVmesure++;
      if(nbVmesure==4 && vbat/4<31424){//2,4 V = 31424 ~ 8 V
          led = 0b11111110 & led;//allumage derniere led
          Write_PCF8574(0x40, led);
          //shut down moteur & reinit
          dutyCycleMoteurD=0;
          dutyCycleMoteurG=0;
          dutyCycleServo=99.99;
          timer1ticks=0;
          T1CONbits.TMR1ON=0;
          marche=0;
      }
      if(nbVmesure==4){
          vreal=vbat/4;
          PIE1bits.TXIE=1;
          vbat=0;
          nbVmesure=0;
      }
    }
   //Timer 2
   if(PIR1bits.TMR2IF){
        PIR1bits.TMR2IF=0;
   }
   //Timer 1
   if(PIR1bits.TMR1IF){
       timer1ticks++;
       PIR1bits.TMR1IF=0;
       if(PORTCbits.RC5==0){
        //1400 ticks pour une p�riode qui correspond aux -90, +90 limites
        Tmr1StartTime=(65536-(int)(19500*(dutyCycleServo)));//calcul de p�riode haute
        TMR1H = (Tmr1StartTime>>8) & 0xff;//poids fable
        TMR1L = Tmr1StartTime&(0xff);//poids fort
        PORTCbits.RC5 = 1;
       }
       else{
        Tmr1StartTime=(int)(65536-(int)(19500*(unsigned float)(1.0 - dutyCycleServo)));
        TMR1H = (Tmr1StartTime>>8) & 0xff;
        TMR1L =Tmr1StartTime&(0xff);
        PORTCbits.RC5 = 0;
       }

   }
}
