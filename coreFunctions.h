/* 
 * File:   coreFunctions.h
 * Author: cheva
 *
 * Created on 9 mai 2019, 21:53
 */

#ifndef COREFUNCTIONS_H
#define	COREFUNCTIONS_H

int detection(void);
int orientation(int angle);
int avancer(void);
void ecrireChar(char c[30]);
void ecrireInt( int k);

#endif	/* COREFUNCTIONS_H */

