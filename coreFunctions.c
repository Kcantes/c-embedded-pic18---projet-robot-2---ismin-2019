/*
 * File:   coreFunctions.c
 * Author: cheva
 *
 * Created on 9 mai 2019, 21:53
 */

#include <p18cxxx.h>
#include "global.h"
#include "MI2C.h"
#include <stdlib.h>
#include <stdio.h>
int dist=0;

int detection(void);
int orientation(int angle);
int avancer(void);
void ecrireChar(char c[30]);
void ecrireInt( int k);

void tempo(unsigned int T){
    unsigned int j;
    for(j=0;j<T;j++){
        
    }
}

//message uart
void ecrireChar(char c[30]){
    printf("%s",c);
}

void ecrireInt( int k){
    printf("%d\r\n",k);
}

int detection(void){
    char message1[30]="detection, ";//message
    char message2[30]="Angle trouv� : ";//message
    char message3[30]="Angle non trouv�...\r\n";//message
    int temp=0,i_inc=0;
    int angleRepere=400;
    
    Tmr1StartTime=64611;
    dutyCycleServo=0.11;
    timer1ticks=0;
    T1CONbits.TMR1ON=1;//launch timer1
    ecrireChar(message1);

    //180/2.5=72
    while(i_inc<72){
        
        if(marche==0)
        {
            T1CONbits.TMR1ON=0;
            return 400;
        }
        if(timer1ticks==5){//on attends que �a tourne
            ecrireInt((int)(98*dutyCycleServo*20-118));//message
            SONAR_Write(0xE0,0x00);
            SONAR_Write(0xE0,0x51);
            tempo(140000);//Attente pour le write >72ms
            temp=SONAR_Read(0xE0,0x02);//read
            timer1ticks++;
        }
        

        if(timer1ticks==16){//on s'assure d'avoir attendu plus que 72ms et on ne veut que cela ne sexecute qu'une fois
            
            timer1ticks++;
            //dist=temp/256;
            //temp=temp-dist*256;
            ecrireInt(temp);
            if(temp>50){
                led=0b01111111;
            }
            if(temp>100){
                led=0b00111111;
            }
            if(temp>150){
                led=0b00011111;
            }
            if(temp>180){
                led=0b00001111;
            }
            if(temp>190){
                led=0b00000111;
            }
            if(temp>200){
                led=0b00000011;
            }
            if(temp>210){
                led=0b00000001;
            }
            if(temp>250){
                led=0b00000001;
            }
            Write_PCF8574(0x40, led);
            if(temp>192&&temp<208&& angleRepere==400){//~2 meters
                ecrireChar(message2);//message
                led = 0b11111111;
                Write_PCF8574(0x40, led);//allumage premiere led
                //angleRepere=(int)(135.85*dutyCycleServo*20-208.87);//angle 180/100=1.8
                angleRepere=(int)(98*dutyCycleServo*20-118);
                //return angleRepere;
                ecrireInt(angleRepere);//message
        }
        }
        if(timer1ticks>18){//encore un tic
            timer1ticks=0;//reset ticks count
            //dutyCycleServo-=0.000868;//decrease duty cycle
            dutyCycleServo-=0.0013;
            if(dutyCycleServo<0)
                dutyCycleServo=0;
            i_inc++;//next angle
        }
    }
    T1CONbits.TMR1ON=0;
    led = 0b10000000 | led;
    Write_PCF8574(0x40, led);//extinction premiere led
    if(angleRepere==400)
        ecrireChar(message3);//message
    return angleRepere;
}

int orientation(int angle){
    char message1[30]="orientation\r\n";//message
    long n=0;//compteur de tick d'acquisition moteur
    long ticknumber=100*(angle-10);//length unit
    led = 0b10111111 & led;
    Write_PCF8574(0x40, led);//allumage deuxieme led
    ecrireChar(message1);//message
    n=0;
    if(ticknumber<0)
        ticknumber*=-1;//ticknumber to be positive, angle might be<0
    if(angle==0)
        return 1;//skip go forward
    dutyCycleMoteurD = 26;
    dutyCycleMoteurG = 26;
    if(angle>0){//tourne � droite
        PORTAbits.RA6=1;
        PORTAbits.RA7=1;
    }
    else if(angle<0){//tourne a gauche
            PORTAbits.RA6=0;
            PORTAbits.RA7=0;
            dutyCycleMoteurD = 26;
            dutyCycleMoteurG = 26;
    }
    
    CCPR1L = dutyCycleMoteurD * 2.5;
    CCPR2L = dutyCycleMoteurG * 2.5;
    while(n<ticknumber){//turn a certain time to be defined
 /*       if(PORTCbits.RC0+PORTAbits.RA4>0){
                n=n+1;
                while(PORTCbits.RC0||PORTAbits.RA4);
            }*/
        n=n+1;
        tempo(25);
        if(marche==0)//reset & exit
        {
            dutyCycleMoteurD = 0;
            dutyCycleMoteurG = 0;
            CCPR1L = dutyCycleMoteurD * 2.5;
            CCPR2L = dutyCycleMoteurG * 2.5;
           return(-1);
        }
    }
    dutyCycleMoteurD = 0;//reinit moteurs
    dutyCycleMoteurG = 0;
    CCPR1L = dutyCycleMoteurD * 2.5;
    CCPR2L = dutyCycleMoteurG * 2.5;
    led = 0b01000000 | led;
    Write_PCF8574(0x40, led);//extinction deuxieme led
    return 1;
}
//idem que orientation, extinction des ports Dird et Dirg au pr�alable, une attente de fin d'inertie peut �tre n�cessaire
int avancer(void){
    char message1[30]="avancer\r\n";//message
    long n=0;//compteur de tick d'acquisition moteurs
    long ticknumber=900;//length unit
    ecrireChar(message1);//message
    led = 0b11011111 & led;
    Write_PCF8574(0x40, led);
    PORTAbits.RA6=0;
    PORTAbits.RA7=1;
    dutyCycleMoteurD = 40;
    dutyCycleMoteurG = 40;
    CCPR1L = dutyCycleMoteurD * 2.5;
    CCPR2L = dutyCycleMoteurG * 2.5;
    while(n<ticknumber){
        if(PORTCbits.RC0+PORTAbits.RA4>0){
                n++;
                while(PORTCbits.RC0+PORTAbits.RA4>0);
            }
        if(marche==0){
            dutyCycleMoteurD = 0;
            dutyCycleMoteurG = 0;
            CCPR1L = dutyCycleMoteurD * 2.5;
            CCPR2L = dutyCycleMoteurG * 2.5;
            return(-1);
        }   
    }
    dutyCycleMoteurD = 0;
    dutyCycleMoteurG = 0;
    CCPR1L = dutyCycleMoteurD * 2.5;
    CCPR2L = dutyCycleMoteurG * 2.5;
    led = 0b00100000 | led;
    Write_PCF8574(0x40, led);
    return 1;
}