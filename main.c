/*
 * File:   main.c
 * Author: olait
 *
 * Created on 29 avril 2019, 08:50
 */

#include <stdio.h>
#include <stdlib.h>
#include "MI2C.h"
#include <p18f2520.h>
#include "coreFunctions.h"
#include "interruption.h"
#include "initialisation.h"
#include "global.h"
#pragma config OSC = INTIO67
#pragma config PBADEN = OFF, WDT = OFF, LVP = OFF, DEBUG = ON
/*
 *
 */
//init global vars
unsigned char marche=0,nbVmesure=0;
volatile unsigned long vbat=0;
unsigned int dutyCycleMoteurD=0;
unsigned int dutyCycleMoteurG=0;
unsigned float dutyCycleServo=99.99;
unsigned int timer1ticks=0;
unsigned int Tmr1StartTime=64611;
int tmp=0;
volatile unsigned int led=0b11111111;
char i2c_isUsed=0;

void main(void) {
    Initialisation();
    InitialiserMoteurs();
    marche=0;
    while(1){
        led = 0b11111110 | led;//allumage derniere led
        Write_PCF8574(0x40, led);
        PORTBbits.RB5=0;
        while(marche!=1);
        PORTBbits.RB5=1;
        tmp=detection();
        if(tmp==400)
            continue;
        tmp=orientation(tmp);
        if(tmp==-1)
            continue;
        tmp=avancer();
        if(tmp==-1)
            continue;
        marche=0;
    }
}