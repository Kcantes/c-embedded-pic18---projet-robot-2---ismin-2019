/* 
 * File:   interruption.h
 * Author: cheva
 *
 * Created on 9 mai 2019, 22:02
 */

#ifndef INTERRUPTION_H
#define	INTERRUPTION_H

void HighISR(void);

#endif	/* INTERRUPTION_H */

