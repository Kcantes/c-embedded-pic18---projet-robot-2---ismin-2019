/*
 * File:   initialisation.c
 * Author: cheva
 *
 * Created on 9 mai 2019, 22:12
 */

#include <p18cxxx.h>
#include "global.h"
#include "coreFunctions.h"
#include "MI2C.h"

void Initialisation(){
    OSCCONbits.IRCF=7; //Horloge � 8Mhz
    //On valide les interruptions
    INTCONbits.INT0IE = 1;
    INTCON2bits.INTEDG0 = 0; //Front descendant interruption 0
    //Desactivation des infrarouges
    PORTBbits.RB1 = 1;
    //ADC
    TRISBbits.RB5=0;
    //TRISAbits.RA2=1;
    ADCON1bits.VCFG=0;//Vref- = Vss & Vref+ =VDD
    ADCON1bits.PCFG=12;//Port AN2 opened
    ADCON2bits.ADCS=6;//Fosc/8 1
    ADCON2bits.ACQT=7;//4 TAD 3
    ADCON0bits.CHS=2;//Channel 2
    ADCON2bits.ADFM=0;//Format left justified
    ADCON0bits.ADON=1;//Enable converter
    PIR1bits.ADIF=0;//reset flag
    PIE1bits.ADIE=1;//allow interrupt
    INTCON2bits.INTEDG0 = 0;//Falling Edge
    INTCON2bits.INTEDG1 = 0;//Falling Edge
    ADCON0bits.GO=1;//launch converter
    //peripheral and core interrupt
    INTCONbits.PEIE = 1;
    INTCONbits.GIE = 1;
    //Timer 0
    T0CONbits.TMR0ON=1;
    T0CONbits.T08BIT=0;//16 bits timer
    T0CONbits.T0CS=0;//Clock source internal
    T0CONbits.PSA=0;//prescaler is assigned
    T0CONbits.T0PS=7;//Prescalar selection 1:256
    INTCONbits.TMR0IE=1;//enable it
    TMR0H=0xE1;//Start time (to overflow at 1s)
    TMR0L=0x7B;
    //Initialisation I2C
    MI2CInit();
    
    //Init Timer1
    T1CONbits.RD16=1;//16 bits operation
    T1CONbits.T1RUN=1;//Start timer 1
    T1CONbits.T1CKPS=1;//prescalar=1:2
    T1CONbits.T1OSCEN=0;//Oscillator disabled
    T1CONbits.T1SYNC=0;//Ignored bit
    T1CONbits.TMR1CS=0;//Internal clock
    PIR1bits.TMR1IF=0;
    PIE1bits.TMR1IE=1;
    //periode ~3ms 1400 tics
    TMR1H = 0xFC;
    TMR1L = 0x18;
    //Servomoteur en sortie
    TRISCbits.RC5=0;
    //uart
    TXSTAbits.SYNC=0;//Asynchronous mode
    TXSTAbits.BRGH=1;//High speed
    BAUDCONbits.BRG16=0;//Baud rate 8 bits
    SPBRG=51;
    RCSTAbits.SPEN=1;
    TXSTAbits.TXEN=1;//transmit enable
    PIR1bits.TXIF=0;
    PIE1bits.TXIE=0;
    RCSTAbits.CREN=1;//enables receiver
    
}


void InitialiserMoteurs(void){
    char message[30]="fin init\r\n";//message
    TRISAbits.RA6=0; //DIRD sortie
    TRISAbits.RA7=0; // DIRG sotie
    TRISAbits.RA4=1; //acquisition moteur droit
    TRISCbits.RC0=1; //acquisition moteur gauche
    /*init Timer 2*/
    /* Configuration E/S*/
    TRISCbits.RC1 = 0;          // RC1 en sortie PWM droite
    TRISCbits.RC2 = 0;          // RC2 en sortie PWM gauche
    /* Configuration TIMER2 */
    T2CONbits.T2CKPS0 = 1;
    T2CONbits.T2CKPS1 = 0;      // CLK /4
    PR2 = 499;                  // Reglage periode FPWM = Fosc/(4*(PR2+1)*PRE)
    T2CONbits.T2OUTPS=9;        // postscaler = 9
    /* Reglage rapport cyclique */
    CCP1CONbits.DC1B0 = 0;
    CCP1CONbits.DC1B1 = 0;
    CCP2CONbits.DC2B0 = 0;
    CCP2CONbits.DC2B1 = 0;
    /* Selection Mode PWM */
    CCP1CONbits.CCP1M3 = 1;
    CCP1CONbits.CCP1M2 = 1;
    CCP2CONbits.CCP2M3 = 1;
    CCP2CONbits.CCP2M2 = 1;
    /* Configuration interruption TMR2*/
    PIE1bits.TMR2IE=0;  // Validation TMR2IF (TMR2IP =1 par d�fault)
    INTCONbits.PEIE=1;  //Validation des interruptions des p�riph�riques
    T2CONbits.TMR2ON = 1;    //Lance le moteur
    CCPR1L = dutyCycleMoteurD * 2.5;
    CCPR2L = dutyCycleMoteurG * 2.5;
    ecrireChar(message);//envoie message
    INTCONbits.GIE=1;  // Validation globale des INT
}