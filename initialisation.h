/* 
 * File:   initialisation.h
 * Author: cheva
 *
 * Created on 9 mai 2019, 22:13
 */

#ifndef INITIALISATION_H
#define	INITIALISATION_H

void Initialisation (void);
void InitialiserMoteurs(void);

#endif	/* INITIALISATION_H */

